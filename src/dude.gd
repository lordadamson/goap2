extends CharacterBody2D

const bakery_scene = preload("res://bakery.tscn")
const wind_mill_scene = preload("res://wind_mill.tscn")
const wheat_farm_scene = preload("res://wheat_farm.tscn")

# the goal of every dude is to increase
# happiness
var happiness = 0
var age = 0
var current_goal = ""
var goal_stack = []
const movement_speed: float = 20.0

var interest = []
var danger = []

@onready var navigation_agent: NavigationAgent2D = $NavigationAgent2D

var building_scene = {
	"bakery": bakery_scene,
	"wind_mill": wind_mill_scene,
	"wheat_farm": wheat_farm_scene,
}

# todo randomly mutate these values
# to make sure everyone is a little
# different
const goal_weights = {
	"food": 10
}

var delta = 0
var mined = 0

var goal_costs = {
	"tree": 1,
	"wood": null,
	"wheat_farm": null,
	"wheat": null,
	"wind_mill": null,
	"flour": null,
	"bakery": null,
	"bread": null,
}

const biom = {
	"granery": ["grass", "desert"],
	"wheat_farm": ["grass"],
	"bakery": ["grass", "desert"]
}

const how = {
	"food": ["bread"],
	"bread": ["bakery", "flour"],
	"bakery": ["wood:3"],
	"flour": ["wheat", "wind_mill"],
	"wind_mill": ["wood:3"],
	"wheat": ["wheat_farm"],
	"wheat_farm": ["wood:3"],
	"wood": ["tree"]
}

const effort = {
	"tree": 1,
	"wood": 1,
	"wheat_farm": 3,
	"wheat": 1,
	"wind_mill": 3,
	"flour": 1,
	"bakery": 3,
	"bread": 1,
	"food": 1
}

var inventory = {
	"bread": 0,
	"tree_logs": 0,
	"wood": 0,
	"wheat": 0,
	"flour": 0,
}

var destination = null
var nutrition = 0
const bread_food_exchange = 10
const flour_bread_exchange = 10
const wheat_flour_exchange = 10

func calculate_needed(count, exchange_rate):
	var needed = float(count) / float(exchange_rate)
	needed = ceilf(needed)
	return int(needed)

func food(count):
	print("food", count)
	
	if(count == 0):
		return true
	
	var needed = calculate_needed(count, bread_food_exchange)
	
	if(inventory.bread < needed):
		if(!bread(needed)):
			return false
	
	nutrition += needed * bread_food_exchange
	inventory.bread -= needed
	
	return true

func bread(count):
	print("bread", count)
	
	var needed = calculate_needed(count, flour_bread_exchange)
	
	if(inventory.flour < needed):
		if(!flour(needed)):
			return false
	
	if(!bakery(1)):
		return false
	
	inventory.bread += needed * flour_bread_exchange
	inventory.flour -= 1
	
	return true

func find_building(building_name):
	var buildings = get_node("/root/world/buildings").get_children()
	
	for b in buildings:
		if(b.name.begins_with(building_name)):
			return b
	
	return null

const grid_size = 128

func snip_snap(x):
	return ((int(x)-1)|(grid_size-1))+1

func build_wooden_building(building_name, wood_count):
	var scene = building_scene[building_name]
	var ins = scene.instantiate()
	ins.name = building_name + str(randi())
	ins.z_index = 1
	var query = PhysicsShapeQueryParameters2D.new()
	ins.position.x = snip_snap(position.x)
	ins.position.y = snip_snap(position.y)
	query.set_transform(ins.transform)
	query.set_shape(ins.get_node("CollisionShape2D").shape)
	
	var space_state = get_world_2d().get_direct_space_state()
	var result = space_state.get_rest_info(query) 
	
	while result:
		if(randi_range(0, 1) == 0):
			ins.position.x += grid_size
		else:
			ins.position.x -= grid_size
		
		if(randi_range(0, 1) == 0):
			ins.position.y += grid_size
		else:
			ins.position.y -= grid_size
		query.set_transform(ins.transform)
		result = space_state.get_rest_info(query)
	
	inventory.wood -= wood_count
	get_node("/root/world/buildings").add_child(ins)
	return ins

func _building(building_name, wood_needed, count):
	if(inventory.wood < wood_needed):
		if(!wood(wood_needed)):
			return false
	
	print(building_name, count)
	var destination = find_building(building_name)
	if(destination == null):
		destination = build_wooden_building(building_name, wood_needed)
	
	return move_to(destination)
	
func wheat_farm(count):
	return _building("wheat_farm", 3, count)
	
func bakery(count):
	return _building("bakery", 3, count)

func wind_mill(count):
	return _building("wind_mill", 3, count)

func flour(count):
	print("flour", count)
	var needed = calculate_needed(count, wheat_flour_exchange)
	
	if(inventory.wheat < needed):
		if(!wheat(needed)):
			return false
	
	if(!wind_mill(1)):
		return false
	
	inventory.flour += needed * wheat_flour_exchange
	inventory.wheat -= 1
	
	return true

func wheat(count):
	print("wheat", count)
	
	if(!wheat_farm(1)):
		return false
	
	var destination = find_building("wheat_farm")
	destination.mine()
	mined += 1
	inventory.wheat += 1
	
	if(mined == count):
		mined = 0
		return true
	
	return false

func find_closest_tree():
	var trees = get_node("/root/world/trees")
	
	var min_distance = 999999999
	var closest_tree = null
	
	for t in trees.get_children():
		var distance = self.position.distance_to(t.position)
		if(distance < min_distance):
			min_distance = distance
			closest_tree = t
	
	return closest_tree

func set_interest():
	pass

func set_danger():
	pass

func move_to(destination):
	set_interest()
	set_danger()
	
	navigation_agent.target_position = destination.position
	var current_agent_position: Vector2 = global_position
	var next_path_position: Vector2 = navigation_agent.get_next_path_position()

	var new_velocity: Vector2 = next_path_position - current_agent_position
	new_velocity = new_velocity.normalized()
	new_velocity = new_velocity * movement_speed

	velocity = new_velocity
#	move_and_slide()
#	
#	velocity = destination.position - position
#	velocity = velocity.normalized()
	var collision_info = move_and_collide(velocity * delta * movement_speed)
	
	if collision_info:
		if(collision_info.get_collider() == destination):
			return true
			
#	move_and_slide()
	return false

func tree(count):
	if(destination == null):
		destination = find_closest_tree()
	
	var are_we_there_yet = move_to(destination)
	
	if(are_we_there_yet):
		destination.mine()
		mined += 1
		
		if(inventory.has("tree")):
			inventory.tree_logs += 1
		else:
			inventory.tree_logs = 1
	
	if(mined == count):
		mined = 0
		return true
	
	return false

const tree_log_wood_exchange_rate = 10

func wood(count):
	print("wood", count)
	var needed = calculate_needed(count, tree_log_wood_exchange_rate)
	
	if(inventory.tree_logs < needed):
		if(!tree(needed)):
			return false
	
	inventory.wood += needed * tree_log_wood_exchange_rate
	
	return true

func get_goal_and_count(goal):
	var tmp = goal.split(':')
	assert(tmp.size() == 1 or tmp.size() == 2)
	
	goal = tmp[0]
	var count = 1
	
	if(tmp.size() == 2):
		count = int(tmp[1])
	
	return {"goal": goal, "count": count}

func calculate_effort(goal, count, p_inventory):
	
	var present = 0
	
	if(p_inventory.has(goal)):
		present = p_inventory[goal]
	
	var needed = count - present;
	
	if(needed < 0):
		needed = 0
	
	return needed * effort[goal]

func evaluate_cost(goal, count):
	var cost = 0
	
	calculate_effort(goal, count, inventory)
	
	if(!how.has(goal)):
		return cost
	
	var sub_goals = how[goal]
	
	for s in sub_goals:
		var g = get_goal_and_count(s)
		cost += evaluate_cost(g.goal, g.count)
	
	return cost

func choose_goal():
	var max_score = -99999
	var final_goal = ""
	
	for goal in goal_weights.keys():
		var cost = evaluate_cost(goal, 1)
		var value = goal_weights[goal]
		
		var score = value - cost
		
		if(score > max_score):
			max_score = score
			final_goal = goal
			
	return final_goal

func gen_goal_stack(goal):
	var stack = []
	
	var g = get_goal_and_count(goal)
	stack.append(goal)
	
	if(!how.has(g.goal)):
		return stack
		
	for h in how[g.goal]:
		stack.append_array(gen_goal_stack(h))
	
	return stack

func actor_setup():
	# Wait for the first physics frame so the NavigationServer can sync.
	await get_tree().physics_frame
	
# Called when the node enters the scene tree for the first time.
func _ready():
	# These values need to be adjusted for the actor's speed
	# and the navigation layout.
	navigation_agent.path_desired_distance = 4.0
	navigation_agent.target_desired_distance = 4.0

	# Make sure to not await during _ready.
	call_deferred("actor_setup")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(_delta):
	delta = _delta
	if(current_goal == ""):
		current_goal = choose_goal()
	
	var g = get_goal_and_count(current_goal)
	
	var finished = Callable(self, g.goal).call(g.count)
	
	if(finished):
		current_goal = ""
