extends StaticBody2D

var capacity = 20

func mine():
	capacity -= 1
	if(capacity == 0):
		queue_free()

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass
